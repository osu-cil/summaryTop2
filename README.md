# summaryTop2

summaryTop2 is a Python rebuild of the old topsumm.pl script (see [topsumm_old.pl](topsumm_old.pl)).

It queries a list of hosts for information and displays it in a webpage.

It is currently run as a cron job under the pittmara user on balulu.
