#!/usr/bin/perl

@hosts = qw/
    frehley
    simmons
    stanley
    dudu
    gudea
    rimus
    nanum
    irarum
    kurum
    tirigan
    hablum
    balulu
    ibbisin
    capefalcon
    monarch
    oysterville
    /;
# criss is kinda dead
# oysterville offline

$when = scalar(localtime(time));

print <<EOF;
<html><head><title>System Status Summary</title></head>
<body font-family:"Courier New", Courier, monospace>
<h1 style="font-family: serif">CIL Server Usage Info</h1>
<font face="Courier">
<table cellpadding="5" cellborder="5" border="2" rules="all" frame="all">
<caption>
<b>System Status Summary, $when</b></caption>
<tr>
<th>System</th>
<th>Users</th>
<th>Tasks</th>
<th>Idle %</td>
<th>Uptime</th>
<th>Load Averages</th>
<th>Norm. Load Averages</th>
<th>Users</th>
</tr>
EOF

sub normalizeLoadAvgs {
    my $loadAvgsStr = $_[0];
    my $nproc = $_[1];
    if ( $loadAvgsStr =~  /(\d+\.\d+), (\d+\.\d+), (\d+\.\d+)/) {
        $oneMin = ($1/$nproc);
        $fiveMin = ($2/$nproc);
        $fifteenMin = ($3/$nproc);
        $loadAvgsStr = sprintf("%.2f, %.2f, %.2f", $oneMin, $fiveMin, $fifteenMin);
    } else {die;}
    return ($loadAvgsStr);
}

foreach $h (@hosts) {

    print STDERR "$h \n";
    eval {
        local $SIG{ALRM} = sub{die;};
        $fail = 1;

        open( I, "timeout 10 ssh -q $h cat /etc/redhat-release |" ) || die;

        undef $rel;
        while(<I>) {
            $rel = $1 if /release (\d+\.\d+)/;
        }
        close I;

        undef $nproc;
        open( I, "timeout 10 ssh -q $h grep processor /proc/cpuinfo | tail -n 1 | egrep -o '[0-9]+' |" );
        while(<I>) {
            $nproc = $1 if /(\d+)/;
            $nproc += 1;
        }
        close I;


        open( I, "timeout 10 ssh -q $h top -b -n 1 |" ) || die;

        undef %what;

        alarm(8);  # 30 seconds, pal
        while(<I>) {
            alarm(0);
            chomp;
            last if /^$/;    # end of header
            #next unless /top -/;  # first line of top output
            $what{users} = $1 if /(\d+) user/;
            $what{load} = $1 if /average: (.+)/;
            #$what{up} = $1 if /up (.+:\d\d)/;
            $what{up} = $1 if /up (.+),\s+\d+\s+user/;
            $what{tasks} = $1 if /Tasks: (\d+) to/;
            $what{idle} = $1 if /([\.\d]+)\%id/;
            # change for CentOS 7, stinkers
            $what{idle} = $1 if /\%Cpu.*ni,\s*([\.\d]+)\ id,/;
        }
        $normLoadAvgs = normalizeLoadAvgs($what{load}, $nproc);

        print "<tr>\n";
        $h =~ s/-giga//;
        $h .= '&nbsp;'x(8-length($h));
        print "<td>$h ($rel) ($nproc)</td>\n";
        print "<td align=\"right\">$what{users}</td>\n";
        print "<td align=\"right\">$what{tasks}</td>\n";
        print "<td align=\"right\">$what{idle}</td>\n";
        print "<td>$what{up}</td>\n";
        print "<td>$what{load}</td>\n";
        print "<td>$normLoadAvgs</td>\n";

        undef %users;
        <I>;    # skip "PID USER ..." line
        while(<I>) {
            chomp;
            last if /^$/;
            @d = split(' ');
            $user = $d[1];
            $uid = &lookUpUID( $user );
            next if $uid < 500;
            next if $uid > 2000;
            next if ($user =~ /^(chrony|colord|polkitd)$/);
            next if $user eq 'geoclue';
            #next if $user eq 'stanley';
            $users{$user}++;
            #print STDERR "$d[0] $d[1] $uid $d[$#d]\n";
            $what{"$user $d[$#d]"}++;
        }
        $users{"stanley"}-=3;  # remove 3 processes for ssh,top,csh (launched by this script)

        $last = '';
        foreach $i (sort keys %what) {
            ($user,$task) = split(' ', $i, 2 );
            $count = $what{$i};
            do {
                #print "\n" if $last ne '';
                #print "$user  $task";
                #print "($count)" if $count > 1;
                $last = $user;
                next;
            } if $last ne $user;
            #print " $task";
            #print "($count)" if $count > 1;
        }

        print "<td>";
        foreach $i (sort keys %users) {
            do {
                print "$i\($users{$i}\) ";
            } if $users{$i} > 0;
        }
        print "</td>\n";

        print "</tr>\n";

        alarm(0);
        $fail = 0;
    };

    do {
        alarm(0);
        print "<td>$h</td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>--connection failed--</td></tr>\n";
        #close I;
        next;
    } if $fail;

    $fail = 0;


    #print "\n";
    close I;
}

print "</table>\n";
print "</font>";
print "<font face=serif>\n";
print <<EOF;
<h2>Field Descriptions</h2>
<dl>
<dt>System</dt>
<dd>Server name (RedHat/CentOS version) (number of cores or threads) </dd>
<dt>Users</dt>
<dd>Number of non-system users logged-in</dd>
<dt>Tasks</dt>
<dd>Number of processes running on the system</dd>
<dt>Idle %</dt>
<dd>Percent of time CPU is idle.</dd>
<dt>Uptime</dt>
<dd>Time since the system was started.</dd>
<dt>Load Averages</dt>
<dd>Rough idea of how loaded the system has been in the last 1, 5, &
    15 minutes. The normalized load average, below, is probably more useful. See
    <a href="https://www.howtogeek.com/194642/understanding-the-load-average-on-linux-and-other-unix-like-systems/">this page</a>
    for info on interpretation.</dd>
<dt>Norm. Load Averages</dt>
<dd>Load averages normalized by number of processors. 1.0 approximately indicates
    complete CPU usage.</dd>
<dt>Users</dt><
dd>Non-system users logged-in and the number of processes each is running</dd>
</dl>
EOF
print "</font>\n";
print "</body></html>\n";

sub lookUpUID {

    my $n = shift @_;

    (my $name, my $passwd, my $uid, my $gid) =
        getpwnam( $n );

    return $uid;
}

__END__

