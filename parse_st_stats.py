import re
import shlex
import sys
from typing_extensions import Literal
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Type, TypeVar, Union, overload
if sys.version_info < (3, 9):
    from typing import Callable
else:
    from collections.abc import Callable


SUMMARYTOP_DIR = Path(__file__).parent


class ReError(ValueError):
    def __init__(self, pattern, text) -> None:
        super().__init__(f"pattern '{pattern}' did not work with text '{text}'.")


@overload
def re_tok(pattern: str, text: str, substr_i: int) -> str: ...
@overload
def re_tok(pattern: str, text: str, substr_i: slice) -> Tuple[str, ...]: ...
def re_tok(pattern: str, text: str, substr_i: Union[int, slice]):
    """Get tokens using a regular expression and raise an error if the regex doesn't work.

    Parameters
    ----------
    pattern
        The regex
    text
        Text to parse
    substr_i
        Index or slice of substring(s) to retrieve

    Returns
    -------
    Found substring or substrings

    Raises
    ------
    ReError
        The regex was not successful.
    """
    result = re.search(pattern, text)
    if not result:
        raise ReError(pattern, text)
    return result.groups()[substr_i]


class SummaryTopError(Exception):
    pass


class TopHeaderError(SummaryTopError):
    def __init__(self):
        super().__init__("Some lines are missing from the top header file.")


class SystemInfoFieldError(SummaryTopError):
    def __init__(self, field_name: str):
        super().__init__(f"Field {field_name} is missing from the system info file.")


class SystemInfoCPULineError(SummaryTopError):
    def __init__(self):
        super().__init__(f"A field is missing from a CPU description.")


# class UserProcsFileError(SummaryTopError):
#     def __init__(self, type)


@dataclass
class TopHeaderStats:
    uptime: str
    num_users: int
    load_averages: Tuple[float, float, float]
    num_tasks: int
    percent_idle: float
    mem_kiB_used: int

    @property
    def summaryTop_text(self) -> str:
        ram_used_txt = f"{self.mem_kiB_used/2**20:.1f}"
        return (
            f"Uptime: {self.uptime}"
            f"\n{self.num_users} users | {self.num_tasks} tasks"
            f"\n{self.percent_idle:.0f}% idle | {ram_used_txt} GiB RAM in use"
        )


def parse_top_header_file(top_header_path: Union[Path, str]) -> TopHeaderStats:
    with open(top_header_path) as fobj:
        top_text = fobj.read()
    lines = top_text.splitlines()
    if len(lines) < 4:
        raise TopHeaderError
    stats = {}
    for line in lines:
        if line[:3] == "top":  # first line
            stats["uptime"] = re_tok(r"up +(.+?), +\d+ users?", line, 0)
            stats["num_users"] = int(re_tok(r"(\d+) users?", line, 0))
            load_avgs = re_tok(
                r"load average\: (\d+\.\d+), (\d+\.\d+), (\d+\.?\d*)",
                line,
                slice(None),
            )
            stats["load_averages"] = (
                float(load_avgs[0]),
                float(load_avgs[1]),
                float(load_avgs[2]),
            )
        elif line[:6] == "Tasks:":  # 2nd line
            stats["num_tasks"] = int(re_tok(r"Tasks: (\d+) total", line, 0))
        elif re.match(r"%?Cpu\(s\)\:.+", line):  # 3rd line
            stats["percent_idle"] = float(re_tok(r"(\d+\.\d+)[ %]id", line, 0))
        elif line[:4] == "Mem:":
            stats["mem_kiB_used"] = int(re_tok(r"Mem: .* (\d+)k used", line, 0))
        elif line[:9] == "MiB Mem :":
            stats["mem_kiB_used"] = int(
                float(re_tok(r"MiB Mem *: .+ (\d+\.\d+) used", line, 0)) * 1024
            )
        elif line[:9] == "KiB Mem :":
            stats["mem_kiB_used"] = int(re_tok(r"KiB Mem : .+ (\d+) used", line, 0))
    return TopHeaderStats(**stats)


@dataclass
class CPUInfo:
    id: int
    description: str
    ncores: int
    nthreads: int


@dataclass
class SystemInfo:
    cpu_info: List[CPUInfo]
    ram_kiB: int
    os_name: str

    @property
    def total_cores(self):
        return sum(cpu.ncores for cpu in self.cpu_info)

    @property
    def total_threads(self):
        return sum(cpu.nthreads for cpu in self.cpu_info)

    @property
    def homogenous(self):
        assert len(self.cpu_info) >= 1  # there has to be at least one cpu!
        cpu0 = self.cpu_info[0]
        return (
            len(self.cpu_info) == 1
            or all(
                (
                    cpu0.description == cpu.description
                    and cpu0.ncores == cpu.ncores
                    and cpu0.nthreads == cpu.nthreads
                ) for cpu in self.cpu_info
            )
        )

    @property
    def cpu_text(self):
        if self.homogenous:
            assert len(self.cpu_info) >= 1  # There has to be at least one cpu!
            return (
                f"{len(self.cpu_info)} x {self.cpu_info[0].description}"
                f"\n{self.total_cores} total cores, {self.total_threads} total threads"
            )
        else:
            # CPU descriptions differ in some way
            cpu_lines = []
            total_cores = 0
            total_threads = 0
            for cpu in self.cpu_info:
                cpu_lines.append(
                    f"CPU {cpu.id}: {cpu.description},"
                    f" {cpu.ncores} cores, {cpu.nthreads} threads"
                )
                total_cores += cpu.ncores
                total_threads += cpu.nthreads
            cpu_lines.append(f"{total_cores} total cores, {total_threads} total threads")
            return "\n".join(cpu_lines)

    @property
    def summaryTop_text(self) -> str:
        return "\n".join(
            [self.os_name, self.cpu_text, f"{self.ram_kiB/2**20:.1f} GiB RAM"]
        )


def _parse_cpu_info(info_text: str) -> List[CPUInfo]:
    phrases = shlex.split(info_text)  # splits string respecting quoted substrings
    cpu_phrases = [[]]  # lists of field strings for all CPUs
    cpu_id = 0
    for phrase in phrases:
        if phrase == "|":  # CPU entry delimiter
            cpu_id += 1
            cpu_phrases.append([])
            continue
        cpu_phrases[cpu_id].append(phrase)
    cpus = []
    for cpu_strings in cpu_phrases:  # the strings for each cpu
        if len(cpu_strings) != 7:
            raise SystemInfoCPULineError
        cpus.append(
            CPUInfo(
                int(cpu_strings[1]),
                cpu_strings[2],
                int(cpu_strings[3]),
                int(cpu_strings[5]),
            )
        )
    return cpus


def parse_system_info_file(sysinfo_path: Union[Path, str]) -> SystemInfo:
    sysinfo = {}
    with open(sysinfo_path) as fobj:
        sysinfo_text = fobj.read()
        for line in sysinfo_text.splitlines():
            key, value = line.split(": ", 1)
            if key == "cpu_info":
                sysinfo[key] = _parse_cpu_info(value)
            elif key == "ram_kiB":
                sysinfo[key] = int(value)
            else:
                sysinfo[key] = value
    for field_name in SystemInfo.__dataclass_fields__:
        if field_name not in sysinfo:
            raise SystemInfoFieldError(field_name)
    return SystemInfo(**sysinfo)


@dataclass
class UserProcsums:
    """System usage statistics for some user at some time interval.

    Attributes
    ----------
    uid
        User id
    user
        User name
    nproc
        Number of processes owned by the user
    pcpu
        Sum of %cpu of all processes owned by the user
    rss_MiB
        Sum of resident set size, in MiB, for all processes owned by the user
    pss_MiB
        Sum of proportional set size, in MiB, for all processes owned by the user
    """
    uid: int
    user: str
    nproc: int
    pcpu: float
    rss_MiB: int
    pss_MiB: int


def parse_user_procsums_file(
    user_procs_path: Union[Path, str]
) -> Dict[str, UserProcsums]:
    """Parse a file generated by user_procsums (or a file in the same format with average
    values) into a list of UserProcsums.
    """
    procsums = {}
    with open(user_procs_path) as fobj:
        for line in fobj:
            if "UID" in line:  # header
                continue
            uid, user, nproc, pcpu, rss_MiB, pss_MiB = line.split()
            procsums[user] = UserProcsums(
                int(uid), user, int(nproc), float(pcpu), int(rss_MiB), int(pss_MiB)
            )
    return procsums


def get_nprocs(
    procsums: Dict[str, UserProcsums], uid_filter_fn: Optional[Callable[[int], bool]] = None
) -> List[Tuple[str, int]]:
    """Create a summaryTop table field that lists the number of processes per user, sorted
    in decreasing order. Optionally filter out some uids with uid_filter_fn which returns
    True for every uid that should be included in the output."""
    uid_filter_fn = uid_filter_fn or (lambda uid: True)
    nprocs = [
        (user, procsum.nproc)
        for user, procsum in procsums.items()
        if uid_filter_fn(procsum.uid)
    ]
    nprocs.sort(key=lambda item: item[1], reverse=True)
    return nprocs


def get_ups_field(
    ups_dict: Dict[str, UserProcsums], user: str, fieldname: str, default: Any
) -> Any:
    """Get a value from a field in a dict of UserProcsums, returning some default value if
    there is no entry in the dict for that username."""
    try:
        return getattr(ups_dict[user], fieldname)
    except KeyError:
        return default


NumberT = TypeVar("NumberT", int, float)


def get_user_averages(
    avg_1min: Dict[str, UserProcsums],
    avg_5min: Dict[str, UserProcsums],
    avg_15min: Dict[str, UserProcsums],
    user_uids: Dict[str, int],
    field_name: str,
    type_: Type[NumberT],
    *,
    min_value: NumberT = 0,
    uid_filter_fn: Optional[Callable[[int], bool]] = None,
    sort_col_i: Literal[0, 1, 2, 3] = 1,
) -> List[Tuple[str, NumberT, NumberT, NumberT]]:
    uid_filter_fn = uid_filter_fn or (lambda uid: True)
    user_rows = [
        (
            user,
            type_(get_ups_field(avg_1min, user, field_name, 0.0)),
            type_(get_ups_field(avg_5min, user, field_name, 0.0)),
            type_(get_ups_field(avg_15min, user, field_name, 0.0)),
        )
        for user in user_uids
        if uid_filter_fn(user_uids[user])
    ]
    # filter again, for min value
    user_rows = [
        row for row in user_rows
        if row[1] >= min_value or row[2] >= min_value or row[3] >= min_value
    ]
    # sory by the sort column
    user_rows.sort(key=lambda row: row[sort_col_i], reverse=True)
    return user_rows


    # if normalize:
    #     # Normalize by max number of threads on the system
    #     user_rows = [
    #         (row[0], row[1]/nthreads, row[2]/nthreads, row[3]/nthreads)
    #         for row in user_rows
    #     ]


    # return "\n".join(
    #     f"{row[0]}: {row[1]:.0f} {row[2]:.0f} {row[3]:.0f}" for row in user_rows
    # )


@dataclass
class SummaryTopRow:
    hostname: str
    system_info: str
    top_header_info: str
    load_averages: Tuple[float, float, float]
    normalized_load_averages: Tuple[float, float, float]
    user_nprocs: List[Tuple[str, int]]  # just 1min avgs
    user_pcpu_averages: List[Tuple[str, float, float, float]]
    user_rss_MiB_averages: List[Tuple[str, int, int, int]]
    user_pss_MiB_averages: List[Tuple[str, int, int, int]]


def first_glob_match(path: Path, pattern: str) -> Optional[Path]:
    for found in path.glob(pattern):
        return found
    return None


def create_summaryTop_row(
    base_stats_dir: Union[Path, str], hostname: str
) -> SummaryTopRow:
    stats_dir = Path(base_stats_dir) / hostname
    system_info = parse_system_info_file(stats_dir / "system_info.txt")
    top_header_stats = parse_top_header_file(list(stats_dir.glob("top_header*.txt"))[0])
    avg1, avg5, avg15 = top_header_stats.load_averages
    nthreads = system_info.total_threads
    norm_load_avgs = (avg1/nthreads, avg5/nthreads, avg15/nthreads)
    avg_1min_file = first_glob_match(stats_dir, "*_avg_1min.txt")
    avg_1min = parse_user_procsums_file(avg_1min_file) if avg_1min_file else {}
    avg_5min_file = first_glob_match(stats_dir, "*_avg_5min.txt")
    avg_5min = parse_user_procsums_file(avg_5min_file) if avg_5min_file else {}
    avg_15min_file = first_glob_match(stats_dir, "*_avg_15min.txt")
    avg_15min = parse_user_procsums_file(avg_15min_file) if avg_15min_file else {}
    user_uids = {
        item[0]: item[1].uid
        for item in {**avg_1min, **avg_5min, **avg_15min}.items()
    }
    pcpu_averages = get_user_averages(
        avg_1min, avg_5min, avg_15min, user_uids, "pcpu", float
    )
    rss_averages = get_user_averages(
        avg_1min, avg_5min, avg_15min, user_uids, "rss_MiB", int, min_value=1024
    )
    pss_averages = get_user_averages(
        avg_1min, avg_5min, avg_15min, user_uids, "pss_MiB", int, min_value=1024
    )
    return SummaryTopRow(
        hostname=hostname,
        system_info=system_info.summaryTop_text,
        top_header_info=top_header_stats.summaryTop_text,
        load_averages=top_header_stats.load_averages,
        normalized_load_averages=norm_load_avgs,
        user_nprocs=get_nprocs(avg_1min),
        user_pcpu_averages=pcpu_averages,
        user_rss_MiB_averages=rss_averages,
        user_pss_MiB_averages=pss_averages,
    )