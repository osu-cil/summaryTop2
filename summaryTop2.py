import getpass
import itertools
import logging
import re
import shlex
import subprocess
import sys
import textwrap
from collections import Counter, defaultdict
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple, Union, overload, TYPE_CHECKING

class HtmlMonoFormatter(logging.Formatter):
    """Formatter that wraps all output in HTML <pre> & <samp> tags"""
    def format(self, ei):
        return f"<pre><samp>\n{super().format(ei)}\n</samp></pre>\n"


logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
base_format = "%(name)s %(levelname)s: %(message)s"
formatter = logging.Formatter(base_format)
html_mono_formatter = HtmlMonoFormatter(base_format)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.ERROR)

if sys.version_info.major < 3 or sys.version_info.minor < 6:
    raise RuntimeError("This version of Python is too old to run this script.")


NORM_LOAD_AVG_WARN = 0.6
NORM_LOAD_AVG_CRIT = 0.85
TIMEOUT_S = 15
RETRY_INCREMENT_S = 5
NTRIES = 2
FILTERED_UNAME_LIST = [
    "chrony",
    "colord",
    "polkitd",
    "geoclue",
    "libstoragemgmt",
    "dnsmasq",
    "pcp",
    "avahi",
    "olsont",
]


@dataclass
class TopStats:
    """Overall system statistics from top"""

    num_users: int
    load_averages: Tuple[float, float, float]
    uptime: str
    num_tasks: int
    percent_idle: float
    normalized_load_averages: Tuple[float, float, float]


@dataclass
class TopSummary:
    """System info the goes in a row of summaryTop

    Attributes
    ----------
    distro_release
        Name of the host's Linux distribution
    num_cpus
        Number of logical cores on the system.
        (sockets*cores_per_socket*threads_per_core)
    cpu_model
        Model name of the CPU
    stats
        Overall system statistics
    user_nprocs
        Number of processes for each non-system user
    user_rss_kB
        Resident set size in kB for each non-system user
    """

    distro_release: str
    num_cpus: int
    cpu_model: str
    stats: TopStats
    user_nprocs: Dict[str, int]
    user_rss_kB: Dict[str, int]


@dataclass
class HostSummary:
    """
    Basically a tuple of hostname and either a TopSummary or a reason we couldn't get
    data
    """

    hostname: str
    summary: Union[TopSummary, str] = ""


def ping_check(hostname):
    """Check that a host is pingable (online)"""
    cmd = shlex.split(f"ping -c 1 -W 1 {hostname}")
    proc = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if proc.returncode == 0:
        return "OK"
    if "100% packet loss" in proc.stdout.decode("UTF-8"):
        return "unreachable"
    else:
        return proc.stderr.decode("UTF-8")


if TYPE_CHECKING:
    CompletedProcess = subprocess.CompletedProcess[bytes]
else:
    CompletedProcess = subprocess.CompletedProcess

def run_remote_command(
    hostname: str,
    command: str,
    timeout_s: int = TIMEOUT_S,
    retry_increment_s: int = RETRY_INCREMENT_S,
) -> Optional[CompletedProcess]:
    """Run a command on a remote host, and return the return code, stdout, and stderr.

    Returns the CompletedProcess or, if the command times out NTRIES, None.
    """
    ntries = NTRIES
    for try_i in range(ntries):
        cmd = shlex.split(f"timeout {timeout_s} ssh -q {hostname} '{command}'")
        proc = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if proc.returncode == 124:
            if try_i < (ntries - 1):
                logger.info(
                    "Timeout executing '%s' on %s. Trying again with %d more seconds.",
                    command,
                    hostname,
                    retry_increment_s,
                )
                timeout_s += retry_increment_s
                continue
            logger.warning("Timeout executing command %s for %s:", command, hostname)
            return None
        return proc


def format_term_text(text: str, indent_nchars: int, wrap_nchars: int, indent_char=" "):
    """
    Format some text with a certain number of indent chars and wrapping at a specific
    level.
    """
    return textwrap.fill(
        text,
        wrap_nchars,
        initial_indent=indent_char * indent_nchars,
        subsequent_indent=indent_char * indent_nchars,
    )


def log_bad_command_result(proc: subprocess.CompletedProcess, hostname: str):
    """Report a bad result from a remote command"""
    logger.error("Error getting top results for %s:", hostname)
    logger.error("--return code: %d", proc.returncode)
    if proc.stdout:
        logger.error(
            "--stdout--\n%s",
            format_term_text(proc.stdout.decode("utf-8"), 4, 80, "-"),
        )
    if proc.stderr:
        logger.error(
            "--stderr--\n%s",
            format_term_text(proc.stderr.decode("utf-8"), 4, 80, "-"),
        )


def get_top(hostname: str) -> Optional[str]:
    """Get the header of the top command"""
    proc = run_remote_command(hostname, "top -b -n 1 | head -n6")  # just get header
    if not proc:
        return None
    if proc.returncode != 0:
        log_bad_command_result(proc, hostname)
        return None
    return proc.stdout.decode("utf-8")


def get_proc_users(hostname: str) -> Optional[List[str]]:
    """
    Get the username for each process on the system -- to determine how many processes
    each user is running.
    """
    proc = run_remote_command(hostname, "ps -e -o user:20 --no-headers")
    if not proc:
        return None
    if proc.returncode != 0:
        log_bad_command_result(proc, hostname)
        return None
    return proc.stdout.decode("utf-8").splitlines()


def get_user_resident_set_size(hostname: str) -> Optional[Dict[str, int]]:
    """Get the resident set size (in kB) for all processes for all users on a system"""
    proc = run_remote_command(hostname, "ps -e -o user:20,rss --no-headers")
    if not proc:
        return None
    if proc.returncode != 0:
        log_bad_command_result(proc, hostname)
        return None
    users_resident_set_size = defaultdict(lambda: 0)
    for line in proc.stdout.decode("utf-8").splitlines():
        username, resident_set_size = line.split()
        users_resident_set_size[username] += int(resident_set_size)
    return users_resident_set_size


def get_num_processors(hostname: str) -> Optional[int]:
    """Get the number of logical cores"""
    proc = run_remote_command(hostname, "grep processor /proc/cpuinfo | wc -l")
    if not proc:
        return None
    if proc.returncode != 0:
        log_bad_command_result(proc, hostname)
        return None
    return int(proc.stdout.decode("utf-8"))


def get_cpu_model(hostname: str) -> Optional[str]:
    proc = run_remote_command(hostname, "cat /proc/cpuinfo")
    if not proc:
        return None
    if proc.returncode != 0:
        log_bad_command_result(proc, hostname)
        return None
    for line in proc.stdout.decode("utf-8").splitlines():
        if "model name" in line.lower():
            name = line.split(":")[1]
            return " ".join(name.split())  # converts all nspaces to single


def get_distro_release(hostname: str) -> str:
    """Get the Linux distribution and release"""
    redhat_release_proc = run_remote_command(hostname, "cat /etc/redhat-release")
    if not redhat_release_proc:
        return ""
    if redhat_release_proc.returncode != 0:
        # try getting os-release instead
        os_release_proc = run_remote_command(
            hostname, "source /etc/os-release && echo $NAME release $VERSION"
        )
        if not os_release_proc:
            return ""
        if os_release_proc.returncode != 0:
            log_bad_command_result(os_release_proc, hostname)
            return "<i>undetermined</i>"
        return os_release_proc.stdout.decode("utf-8").strip()
    return redhat_release_proc.stdout.decode("utf-8").strip()


def get_uids(hostname: str, usernames: List[str]) -> Dict[str, int]:
    """Get numeric UIDs for a list of usernames"""
    cmd = f"for name in {' '.join(usernames)}; do echo $name $(id -u $name); done"
    proc = run_remote_command(hostname, cmd)
    if not proc or proc.returncode != 0:
        return {}
    uids = {}
    for line in proc.stdout.decode("utf-8").splitlines():
        if " " in line:
            username, uid = line.split(" ")
            uids[username] = int(uid)
        else:
            # Maybe no username for this uid
            try:
                uid = int(line)
            except ValueError:
                # can't do anything with this
                continue
            uids[line] = uid
    return uids


@overload
def re_tok(pattern: str, text: str, substr_i: int) -> str:
    ...


@overload
def re_tok(pattern: str, text: str, substr_i: slice) -> Tuple[str, ...]:
    ...


def re_tok(pattern: str, text: str, substr_i: Union[int, slice]):
    """Get tokens using a regular expression and raise an error if the regex doesn't work"""
    result = re.search(pattern, text)
    if not result:
        raise ValueError(f"pattern '{pattern}' did not work with text '{text}'")
    return result.groups()[substr_i]


def parse_top_stats(top_text: str, num_processors: int) -> TopStats:
    """Parse the top command header"""
    lines = top_text.splitlines()
    uptime = ""
    nusers = 0
    load_avgs = (0.0, 0.0, 0.0)
    ntasks = 0
    idle_pct = 0
    for line in lines:
        if line[:3] == "top":
            uptime = re_tok(r"up +(.+?), +\d+ users?", line, 0)
            nusers = int(re_tok(r"(\d+) users?", line, 0))
            load_avgs = re_tok(
                r"load average\: (\d+\.\d+), (\d+\.\d+), (\d+\.?\d*)",
                line,
                slice(None),
            )
            load_avgs = (float(load_avgs[0]), float(load_avgs[1]), float(load_avgs[2]))
        if line[:5] == "Tasks":
            ntasks = int(re_tok(r"Tasks: (\d+) total", line, 0))
        if re.match(r"%?Cpu\(s\)\:.+", line):
            idle_pct = float(re_tok(r"(\d+\.\d+)[ %]id", line, 0))
    norm_load_avgs = (
        load_avgs[0] / num_processors,
        load_avgs[1] / num_processors,
        load_avgs[2] / num_processors,
    )
    return TopStats(nusers, load_avgs, uptime, ntasks, idle_pct, norm_load_avgs)


def _filter_users_dict(users_dict: Dict[str, Any], hostname):
    uids = get_uids(hostname, list(users_dict.keys()))
    for username in list(users_dict.keys()):
        if (
            uids[username] < 500
            or users_dict[username] < 1
            or username in FILTERED_UNAME_LIST
        ):
            del users_dict[username]


def filter_user_nprocs(user_nprocs: Dict[str, int], hostname: str):
    """Filter the user_nprocs dict for system users"""
    my_un = getpass.getuser()
    user_nprocs[my_un] -= 3  # remove 3 for sshd, bash, ps for me
    _filter_users_dict(user_nprocs, hostname)


def filter_user_rss(user_rss: Dict[str, int], hostname: str):
    """Filter out system users from user_rss dict"""
    _filter_users_dict(user_rss, hostname)


def get_top_summary(hostname: str) -> HostSummary:
    """Get all the data to display in summaryTop for a given host"""
    ping_result = ping_check(hostname)
    if ping_result != "OK":
        return HostSummary(hostname, ping_result)
    top_text = get_top(hostname)
    proc_users = get_proc_users(hostname)
    user_rss_kB = get_user_resident_set_size(hostname)
    num_cpus = get_num_processors(hostname)
    cpu_model = get_cpu_model(hostname)
    distro_release = get_distro_release(hostname)
    if top_text is None or num_cpus is None or proc_users is None or user_rss_kB is None or cpu_model is None:
        # Some timeout or error
        return HostSummary(hostname, "ERROR")
    top_stats = parse_top_stats(top_text, num_cpus)
    user_nprocs = Counter(proc_users)
    filter_user_nprocs(user_nprocs, hostname)
    filter_user_rss(user_rss_kB, hostname)
    return HostSummary(
        hostname,
        TopSummary(
            distro_release=distro_release,
            num_cpus=num_cpus,
            cpu_model=cpu_model,
            stats=top_stats,
            user_nprocs=user_nprocs,
            user_rss_kB=user_rss_kB,
        ),
    )


def format_load_avgs(load_avgs: Tuple[float, float, float], percent=False):
    """Format a tuple of floats to two decimal places"""
    if percent:
        return f"{' '.join(f'{avg*100:.0f}%' for avg in load_avgs)}"
    else:
        return f"{' '.join(f'{avg:.2f}' for avg in load_avgs)}"


def gen_host_row(result: HostSummary):
    """Generate an HTML table row with the data from a host"""
    if isinstance(result.summary, TopSummary):
        summary = result.summary
        user_nproc_sorted = sorted(
            summary.user_nprocs.items(), key=lambda item: item[1], reverse=True
        )
        user_nproc_summary = "<br/>".join(
            f"{name:10s} {nproc}" for name, nproc in user_nproc_sorted
        ).replace(" ", "&nbsp;")
        user_rss_sorted = sorted(
            summary.user_rss_kB.items(), key=lambda item: item[1], reverse=True
        )
        user_rss_summary = "<br/>".join(
            f"{name:10s} {rss_kb/1e6:.1f}"
            for name, rss_kb in user_rss_sorted
            if rss_kb >= 500e3
        ).replace(" ", "&nbsp;")
        if summary.stats.normalized_load_averages[0] > NORM_LOAD_AVG_CRIT:
            la_class = ' class="red-bg"'
        elif summary.stats.normalized_load_averages[0] > NORM_LOAD_AVG_WARN:
            la_class = ' class="orange-bg"'
        else:
            la_class = ""
        row = f"""
              <tr>
                  <td>{result.hostname}</td>
                  <td>{summary.distro_release}<br/>
                      {summary.cpu_model}</br/>
                      {summary.num_cpus} logical cores
                  </td>
                  <td>{summary.stats.num_users}</td>
                  <td>{summary.stats.num_tasks}</td>
                  <td>{summary.stats.percent_idle:.1f}</td>
                  <td>{summary.stats.uptime}</td>
                  <td>{format_load_avgs(summary.stats.load_averages)}</td>
                  <td{la_class}>{format_load_avgs(summary.stats.normalized_load_averages, True)}</td>
                  <td>{user_nproc_summary}</td>
                  <td>{user_rss_summary}</td>
              </tr>"""
    else:
        row = f"""
            <tr>
                <td>{result.hostname}</td>
                <td style="font-style: italic">--{result.summary}--</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>"""
    return textwrap.dedent(row)


def find_host_group(hostname: str, host_groups: Dict[str, List[str]]) -> Optional[str]:
    """Determine which host group a host is in"""
    for group_name, group_hosts in host_groups.items():
        if hostname in group_hosts:
            return group_name
    raise ValueError(f"{hostname} is not in a host group")


def gen_webpage(
    host_groups: Dict[str, List[str]], host_summaries: List[HostSummary]
) -> str:
    """
    Generate a webpage from the data collected from all the hosts. The host_groups dict
    determines which group each host is in.
    """
    gen_time = datetime.now().strftime("%a %b %d %H:%M:%S %Y")
    group_rows = defaultdict(list)
    for host_summary in host_summaries:
        host_row = gen_host_row(host_summary)
        host_group = find_host_group(host_summary.hostname, host_groups)
        group_rows[host_group].append(host_row)
    all_rows = "".join(
        [
            textwrap.dedent(
                f"""\n<tr><th colspan=10>{group_name}</th></tr>
                {"".join(group_rows)}"""
            )
            for group_name, group_rows in group_rows.items()
        ]
    )

    return textwrap.dedent(
        f"""
        <!DOCTYPE html>
        <html lang="en/US">
        <head>
            <title>CIL Server Status Summary</title>
            <meta http-equiv="refresh" content="60">
            <style type="text/css">
                table {{
                    border-collapse: collapse;
                    border-style: hidden;
                    font-family: monospace;
                    font-size: 16px;
                }}
                table td, table th {{
                    border: 1px solid black;
                    padding: 5px;
                }}
                td:nth-child(2) {{
                    font-size: smaller;
                }}
                td:nth-child(3), td:nth-child(4), td:nth-child(5) {{
                    text-align: right;
                }}
                td:nth-child(7), td:nth-child(8) {{
                    text-align: center;
                }}
                .orange-bg {{
                    background-color: rgb(255, 178, 85);
                }}
                .red-bg {{
                    background-color: rgb(255, 85, 85);
                }}
                dl {{
                    max-width: 800px;
                }}
                dt {{
                    font-weight: bold;
                }}
            </style>
        </head>
        <body>
        <h1>CIL Server Usage Info</h1>
        <details>
        <summary style="font-size: 16pt; font-weight: bold">
            Click here for field descriptions
        </summary>
            <h2>Field Descriptions</h2>
            <dl>
                <dt>Hostname</dt>
                <dd>Server name</dd>
                <dt>System Info</dt>
                <dd>Name and version of the OS, CPU model, and total number of logical cores</dd>
                <dt>Users</dt>
                <dd>Number of non-system users logged-in (This may be different than the
                    number of users running processes.)</dd>
                <dt>Tasks</dt>
                <dd>Number of processes running on the system</dd>
                <dt>Idle %</dt>
                <dd>Percent of time CPU is idle.</dd>
                <dt>Uptime</dt>
                <dd>Time since the system was started.</dd>
                <dt>Load Averages</dt>
                <dd>Rough idea of how loaded the system has been in the last 1, 5, & 15
                    minutes. The normalized load average, below, is probably more useful.
                    See <a href="https://www.howtogeek.com/194642/understanding-the-load-average-on-linux-and-other-unix-like-systems/">this page</a>
                    for info on interpretation.</dd>
                <dt>Norm. Load Averages (%)</dt>
                <dd>Load averages normalized by number of processing units. 100
                    approximately indicates complete usage of all units. This will be
                    highlighted orange if it goes over {NORM_LOAD_AVG_WARN*100:.0f}, and red
                    if it goes over {NORM_LOAD_AVG_CRIT*100:.0f}.</dd>
                <dt>Users (nprocs)</dt>
                <dd>Non-system users with active processes and the number of processes each is
                    running</dd>
                <dt>User mem used (GB)</dt>
                <dd>Total memory used by each user's processes, if over 500 MB</dd>
            </dl>
        </details>
        <table>
            <caption style="font-weight: bold; font-size: 20pt;">
                System Status Summary {gen_time}
            </caption>
            <thead>
            <tr>
                <th>Hostname</th>
                <th>System Info</th>
                <th>Users</th>
                <th>Tasks</th>
                <th>Idle %</td>
                <th>Uptime</th>
                <th>Load Averages</th>
                <th>Norm. Load Averages (%)</th>
                <th>Users (nprocs)</th>
                <th>User mem used (GB)</th>
            </tr>
            </thead>
            <tbody>
            {textwrap.indent(all_rows, " "*12)}
            </tbody>
        </table>
        </body>
        </html>
        """
    )


def main() -> Optional[str]:
    host_groups = {
        "Rocky 8 Compute Hosts": [
            "capefalcon",
            "oysterville",
            "monarch",
        ],
        "Rocky 8 Non-Compute Hosts": [
            "ibbisin",
        ],
        "CentOS 7 Compute Hosts": [
            "dudu",
            "gudea",
            "nanum",
            "rimus",
            "irarum",
        ],
        "CentOS 7 Non-Compute Hosts": [
            "balulu",
            "stanley",
        ],
        "CentOS 5 Hosts": [
            "frehley",
            "simmons",
            "hablum",
            "kurum",
            "tirigan",
            "attila",
            "shusin",
        ],
    }
    if "--html-mono-logging" in sys.argv:
        handler.setFormatter(html_mono_formatter)
    all_hosts = list(itertools.chain.from_iterable(host_groups.values()))

    try:
        with ThreadPoolExecutor() as executor:
            host_summaries = list(executor.map(get_top_summary, all_hosts, timeout=60))
        return gen_webpage(host_groups, host_summaries)
    except Exception as ex:
        logger.error("There was an error with the script: %s", str(ex))
        logger.debug("Exception info:\n", exc_info=sys.exc_info())
    return ""

if __name__ == "__main__":
    result = main()
    if result:
        print(main())
    else:
        sys.exit(1)
