#!/bin/bash
# This function is in its own script file purely because some syntax highlighters get
# screwed up on regex (match) line which makes the rest of the file hard to read.
function get_cpu_info() {
    awk '
        function lrstrip(mystr) {
            sub(/^[ \t]+/, "", mystr);
            sub(/[ \t]+$/, "", mystr);
            return mystr;
        }
        function wsjoin(mystr) {
            gsub(/[ \t]+/, " ", mystr);
            return mystr;
        }
        BEGIN {
            FS=":";
        }
        {
            fieldname = lrstrip($1);
            value = lrstrip($2);
            if (fieldname == "physical id" || fieldname == "siblings") {printf("%s ", value);};
            if (fieldname == "model name") {printf("\"%s\" ", wsjoin(value));};
            if (fieldname == "cpu cores") {printf("%s\n", value);};
        }' /proc/cpuinfo \
            | sort -n -k2,2 -k3,3 -k4,4 \
            | uniq \
            | awk '
            BEGIN {
                ncpus = 0;
            }
            {
                ncpus++;
                match($0, /"([^"]+)" ([0-9]+) ([0-9]+) ([0-9]+)/, fields);
                # Extract the fields
                cpu_model = fields[1];
                cpu_id = fields[2];
                cores = fields[3];
                threads = fields[4];
                if (ncpus > 1) {printf(" | ")}
                printf("CPU %d \"%s\" %d cores %d threads", cpu_id, cpu_model, cores, threads);
            }
            END {
                printf("\n");
            }'
}

echo "cpu_info: $(get_cpu_info)"
echo "ram_kiB: $(grep MemTotal /proc/meminfo | awk '{print $2}')"
echo -n "os_name: "
if [ -f /etc/redhat-release ] ; then
    cat /etc/redhat-release
elif [ -f /etc/os-release ] ; then
    source /etc/os-release
    echo "$NAME release $VERSION"
else
    echo "UNKNOWN"
fi
