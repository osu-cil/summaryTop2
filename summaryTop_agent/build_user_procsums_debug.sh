#!/bin/bash
rm -f user_procsums.debug
cd src
gcc -Wall -g -ggdb -std=gnu99 -D_GNU_SOURCE pss.c suid_guard.c user_procsums.c -o user_procsums.debug || exit
mv user_procsums.debug ../
cd ../
sudo chown root:root user_procsums.debug && sudo chmod u+s user_procsums.debug
