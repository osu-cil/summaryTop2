#!/bin/bash
set -o pipefail -o nounset
shopt -s nullglob

PROGNAME=$(basename "${BASH_SOURCE[0]}")
PROGDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TSTART=$(date +%s)
readonly PROGNAME PROGDIR TSTART

log() {
    echo >&2 -e "${1:-}"
}

errdie() {
    log "$PROGNAME error: $1"
    exit "${2:-1}"
}

isint() {
    # determine if $1 is an integer
    case "${1-}" in
    '' | *[!0-9]*) return 1 ;;  # empty string or non-digit returns 1
    *) return 0  ;;
    esac
}

check_running() {
    local exg_pids
    # shellcheck disable=SC2207  # ok - just integers
    exg_pids=($(pgrep "$PROGNAME"))
    for pid in "${exg_pids[@]}"; do
        [ $pid -eq $$ ] || errdie \
            "At least one instance of $PROGNAME is already running with PID $pid."
    done
}

readonly LRD_DEFAULT="/tmp/summaryTop_agent"
readonly RRD_DEFAULT='/cil/scripts/summaryTop2/stats/$(hostname -s)'
readonly SI_DEFAULT=5
readonly RUI_DEFAULT=10

usage() {
    cat << EOF
$PROGNAME: $PROGNAME [...]
    Options:
        -h|--help
            Print this text and exit.
        -l|--local_results_dir LOCAL_RESULTS_DIR
            Directory on the local machine for saving the intermediate results, produced
            every SAMPLE_INTERVAL seconds. Defaults to
            "$LRD_DEFAULT".
        -r|--remote_results_dir REMOTE_RESULTS_DIR
            NFS directory to which summary results should be pushed. Defaults to
            "$RRD_DEFAULT".
        -s|--sample_interval
            Number of seconds between each capture of process statistics. Defaults to $SI_DEFAULT.
        -u|--remote-update-interval
            Number of seconds between each upate of the remote results. Defaults to $RUI_DEFAULT.

EOF
}

readonly ARGS=("$@")
readonly NARGS=${#ARGS[@]}
parse_args() {
    local i lrd rrd si rui
    i=0
    lrd="$LRD_DEFAULT"
    rrd=$(eval echo "$RRD_DEFAULT")
    si=$SI_DEFAULT
    rui=$RUI_DEFAULT
    while [ $i -lt $NARGS ]; do
        arg="${ARGS[$i]}"
        case $arg in
            -h | --help) usage; exit 0 ;;
            -l | --local_results_dir)
                lrd="${ARGS[$((i+1))]}"
                ((++i))
                ;;
            -r | --remote_results_dir)
                rrd="${ARGS[$((i+1))]}"
                ((++i))
                ;;
            -s | --sample_interval)
                si="${ARGS[$((i+1))]}"
                ((++i))
                ;;
            -u | --remote-update-interval)
                rui="${ARGS[$((i+1))]}"
                ((++i))
                ;;
            *)
                # set these, in order
                errdie "Invalid argument $arg"
                ;;
        esac
        ((++i))
    done
    # Check values
    ( isint "$si" && [ "$si" -gt 0 ] ) || errdie "Sample interval is not a positive integer"
    ( isint "$rui" && [ "$rui" -gt 0 ] ) || errdie "Remote update interval is not a positive integer"
    # Save to global vars
    readonly LOCAL_RESULTS_DIR="$lrd"
    readonly REMOTE_RESULTS_DIR="$rrd"
    readonly SAMPLE_INTERVAL=$si
    readonly REMOTE_UPDATE_INTERVAL=$rui
}

init_vars() {
    readonly PS_DIR="$LOCAL_RESULTS_DIR/ps"
    mkdir -p "$PS_DIR"
    mkdir -p "$REMOTE_RESULTS_DIR"
    chmod -R a+rwX "$REMOTE_RESULTS_DIR"
    LAST_REMOTE_UPDATE=$((TSTART - REMOTE_UPDATE_INTERVAL))  # always update at start
}

get_top_header() {
    # Remove any existing top_header file, update with new timestamped file
    rm -f "$LOCAL_RESULTS_DIR"/top_header_*.txt
    top -b -n1 |head -n6 > "$LOCAL_RESULTS_DIR/top_header_$(date +%s).txt"
}

get_ps_stats() {
    local ps_file err
    ps_file="$PS_DIR/$(date +%s).txt"
    if ! err=$( { "$PROGDIR/user_procsums" --exclude_self --pids_to_skip=$$ --header > "$ps_file"; } 2>&1 ) ; then
        errdie "$err"
    fi
}

cull_older_than() {
    # With a dir of files ($1), each named <tstamp>_*.txt, where <tstamp> is a UNIX
    # timestamp, deleter all files older than the date string specified in $2. e.g.
    #    cull_older_than /tmp "10 hours ago"
    # or
    #    cull_older_than /tmp "2024-09-21"
    # The time string is anything accepted by `date -d`
    if [ ! -d "$1" ]; then errdie "first arg must be a directory path"; fi
    local oldest_file
    oldest_file=$(date -d "$2" +%s).txt || errdie "Error in date command"
    for f in "$1"/*; do
        if [ -f "$f" ] && [[ "$(basename "$f")" < "$oldest_file" ]] ; then
            rm "$f"
        fi
    done
}

strip_ext() {
    # strip trailing extension from any filename
    echo "${1%.*}"
}

get_tstamp() {
    # get the timestamp from any /abc/def/12345678.txt path
    strip_ext "$(basename "$1")"
}

avg_ps() {
    local avg_s tnow oldest_tstamp all_ps_files files_to_avg
    avg_s=$1
    isint "$avg_s" || errdie "avg_ps requires integer seconds argument"
    tnow=$(date +%s)
    oldest_tstamp=$((tnow - avg_s))
    all_ps_files=("$PS_DIR"/*.txt)
    files_to_avg=()
    for f in "${all_ps_files[@]}"; do
        if [[ $(get_tstamp "$f") -ge $oldest_tstamp ]]; then
            files_to_avg+=("$f")
        fi
    done
    awk '
    FNR == 1 && /USER/ { next }  # skip header lines in each file
    {
        user=$2;
        uid[user] = $1;
        nproc[user] += $3;
        pcpu[user] += $4;
        rss[user] += $5;
        pss[user] += $6
        count[user]++;
    }
    END {
        printf("%8s %20s %10s %10s %11s %11s\n", "UID", "USER", "Avg_NPROC", "Avg_PCPU", "Avg_RSS_MiB", "Avg_PSS_MiB");
        for (user in uid) {
            printf("%8d %20s %10.0f %10.2f %11.0f %11.0f\n", uid[user], user, nproc[user]/count[user], pcpu[user]/count[user], rss[user]/count[user]/1024, pss[user]/count[user]/1024);
        }
    }
    ' "${files_to_avg[@]}"
}

pwatch() {
    # Implementation of 'watch --precise' (when the system `watch` is too old). Executes a
    # command every $1 seconds, checking the time every $2 seconds. If the command takes
    # longer to run than the delay time then it will be run as frequently as possible.
    #
    #  E.g. to print the current time in UTC with nanoseconds every 1 second, checking the
    #  current time every 0.01 seconds:
    #
    #     pwatch 1.0 0.01 date -u +%T.%N
    #
    pwerr() {
        echo "pwatch error: ${1:-}" >&2
    }
    is_number() {
        re='^[0-9]+([.][0-9]+)?$'
        [[ "$1" =~ $re ]]
    }
    local watch_interval watch_check_interval tnow tnext
    is_number "$1" || (pwerr "First arg is pwatch command interval in seconds" && return 1)
    is_number "$2" || (pwerr "Second arg is pwatch check interval in seconds" && return 1)
    watch_interval=$1
    watch_check_interval=$2
    shift 2
    tnow=$(date +%s.%3N)
    tnext=$(bc <<< "scale=3; $tnow + $watch_interval")
    update_tnext() {
        tnext=$(bc <<< "scale=3; $tnext + $watch_interval")
    }
    "$@"  # run command
    while : ; do
        tnow=$(date +%s.%3N)
        # shellcheck disable=SC2046
        if [ $(bc <<< "$tnow >= $tnext") -eq 1 ]; then
            update_tnext
            "$@"  # run command again
        fi
        # shellcheck disable=SC2086
        sleep $watch_check_interval
    done
}

all_avg_ps() {
    local tnow
    tnow=$(date +%s)
    if [ $((tnow - TSTART)) -lt 60 ]; then return; fi
    rm -f "$LOCAL_RESULTS_DIR"/*avg_1min.txt
    avg_ps 60 > "$LOCAL_RESULTS_DIR/${tnow}_avg_1min.txt"
    if [ $((tnow - TSTART)) -lt 300 ]; then return; fi
    rm -f "$LOCAL_RESULTS_DIR"/*avg_5min.txt
    avg_ps 300 > "$LOCAL_RESULTS_DIR/${tnow}_avg_5min.txt"
    if [ $((tnow - TSTART)) -lt 900 ]; then return; fi
    rm -f "$LOCAL_RESULTS_DIR"/*avg_15min.txt
    avg_ps 900 > "$LOCAL_RESULTS_DIR/${tnow}_avg_15min.txt"
}

update_remote() {
    local next_remote_update tnow avg_files
    tnow=$(date +%s)
    next_remote_update=$((LAST_REMOTE_UPDATE + REMOTE_UPDATE_INTERVAL))
    if [[ $tnow -lt $next_remote_update ]]; then return ; fi  # early exit
    rm -f "$REMOTE_RESULTS_DIR"/*.txt
    cp "$LOCAL_RESULTS_DIR/system_info.txt" "$REMOTE_RESULTS_DIR"
    set +u  # don't error on unbound `avg_files` (empty)
    avg_files=("$LOCAL_RESULTS_DIR"/*_avg_*min.txt)
    if [ ${#avg_files[@]} -gt 0 ]; then
        cp "${avg_files[@]}" "$REMOTE_RESULTS_DIR"
    fi
    set -u
    cp "$LOCAL_RESULTS_DIR"/top_header_*.txt "$REMOTE_RESULTS_DIR"
    chmod 444 "$REMOTE_RESULTS_DIR"/*.txt
    LAST_REMOTE_UPDATE=$tnow
}

every_interval() {
    # Each interval of pwatch, make a ps listing, replace the top header, and cull if
    # needed.
    get_ps_stats
    all_avg_ps
    get_top_header
    cull_older_than "$PS_DIR" "15 minutes ago"
    update_remote
}

main() {
    check_running
    parse_args
    init_vars
    "$PROGDIR/get_system_info.sh" > "$LOCAL_RESULTS_DIR/system_info.txt" # only run once at startup
    # Remove any existing (old) avg files
    rm -f "$LOCAL_RESULTS_DIR"/*_avg_*min.txt
    pwatch "$SAMPLE_INTERVAL" 0.1 every_interval
}

main
