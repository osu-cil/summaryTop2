#!/bin/bash
# gcc -std=gnu99 -o user_procsums user_procsums.c
# Build with older GLIBC to make compatiable with CentOS5
cd src
apptainer exec /cil/sw/any/containers/centos5-gcc5_latest.sif gcc -std=gnu99 -D_GNU_SOURCE user_procsums.c pss.c suid_guard.c -o user_procsums -O2
mv user_procsums ../
cd ..
sudo chown root:root user_procsums && sudo chmod u+s user_procsums
