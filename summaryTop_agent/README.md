# summaryTop Agent

summaryTop Agent is a script (`st_agent.sh`) that is run as a service. The purpose of the
agent is to obtain information about a system and save that data to
`/cil/scripts/summaryTop2/stats/<hostname>` (hereafter `$STATSDIR`) whence it can be read
to generate the summaryTop webpage. The information acquired by the summaryTop Agent
comprises the following:

- Static info, at agent startup, in `$STATSDIR/system_info.txt`
  - OS name and version
  - CPU model, \# of CPUs, cores per CPU, threads per core
  - Total ram
- Dynamic info, every (configurable) interval
  - From the `top` header, in `$STATSDIR/top_header_<timestamp>.txt`:
    - Uptime
    - Overall load averages
    - Total CPU usage
    - Total memory usage
  - From the program `user_procsums`, 1, 5, and 15 minute per-user averages of the
    following, in `$STATSDIR/<timestamp>_avg_<n>min.txt` (`<n>` being 1, 5, and 15):
    - Number of processes (NPROC)
    - Percent CPU usage (PCPU)
    - Resident set size (RSS)
    - Proportional set size (PSS)

Of note is the proportional set size, which is the total memory used by a process,
calculated as the private memory of the process plus the _proportion_ of memory shared
with one or more other processes. It is therefore a more accurate representation of the
proportion of a system's memory used by a process, as compared with other processes.

The `user_procsums` program calls the `get_pss()` function that obtains the PSS values
from the `/proc/$pid/smaps_rollup` or `/proc/$pid/smaps` file, as the `ps` command on
current CIL systems does not support retrieving PSS. Since the OS only allows the owning
user or `root` to read the `smaps`/`smaps_rollup` file of a process, the `user_procsums`
must be owned by `root` and given [_setuid_
permissions](https://en.wikipedia.org/wiki/Setuid#When_set_on_an_executable_file) (or
called by root). Guards have been placed in `user_procsums` to ensure that root
permissions are only used when the `smaps`/`smaps_rollup` file is opened and that the
process runs under the caller's uid at all other times.

To install the agent, run `install_service.sh <USER>` where `<USER>` is the username under
which the service should run. This use must be able to write to
`/cil/scripts/summaryTop2/stats`.

## Other files

### `build_user_procsums.sh` and `build_user_procsums_debug.sh`

Scripts to compile `user_procsums` and `user_procsums.debug`, respectively.
`user_procsums` is meant to be run with root suid priviliges and therefore these scripts
include chown and chmod to that effect. `build_user_procsums.sh` uses a CentOS 5 w/GCC
image to build `user_procsums` to allow it to be used on CentOS 5 (attila and shusin). The
Apptainer image was converted from a Docker image from
<https://hub.docker.com/r/jeromerobert/centos5-gcc5>.

### `suid_guard.h`, `suid_guard.c`

Provide functions for setting up and using an "suid guard" that, on setup, sets the
process's effective uid to its uid. "Aquiring" the guard sets the euid to the original
euid, and "releasing" the guard set the euid back to the uid again. Thus any process owned
by root with setuid enable that uses this guard will only get root permissions while the
guard is "acquired".

### `pss.h`, `pss.c`

Provide the `get_pss()` function that gets that PSS for some PID by
reading `/proc/$pid/smaps[_rollup]`. `get_pss()` requires that a program that calls it and
is run with suid privileges (uid != euid) must have the suid guard set up.

### `get_pss.c`

Compiles to `get_pss`. Test program to call `get_pss()` and report the single-process PSS
for any PID.

### `user_procsums.c`

Compiles to `user_procsums`. Runs `ps` and calls `get_pss()` to get
the pcpu, rss, and pss of every running process on the system. Values are summed by
username (along with # of processes) and printed as a fixed-width-column table.

### `summaryTop_agent.service.tmpl`

SystemD service file template (instantiated with `envsubst` on service install) for
running `st_agent.sh` as a service.

### `summaryTop_agent.tmpl`

SysV init script template (instantiated with `envsubst` on service install) for running
`st_agent.sh` as a service (on older CentOS).
