#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )" || echo exit
export STA_USER=$1
if [ -z "$STA_USER" ] || [ "$STA_USER" == "root" ] || ! id "$STA_USER" &>/dev/null; then
    echo "Error: Provide a valid username for the account under which the service will" \
         "run (not root). It should be a user with read/write access to" \
         "/cil/scripts/summaryTop2/stats." >&2
    exit 1
fi

install_systemd_service() {
    envsubst '$STA_USER' < summaryTop_agent.service.tmpl >| summaryTop_agent.service
    sudo systemctl stop summaryTop_agent.service  # if already running
    sudo cp summaryTop_agent.service /etc/systemd/system
    sudo systemctl daemon-reload
    sudo systemctl enable --now summaryTop_agent.service
}

install_initd_service() {
    envsubst '$STA_USER' < summaryTop_agent.tmpl >| summaryTop_agent
    chmod +x summaryTop_agent
    sudo cp summaryTop_agent /etc/init.d/summaryTop_agent
    sudo chkconfig --add summaryTop_agent
    sudo chkconfig summaryTop_agent on
    sudo service summaryTop_agent start
}

main() {
    INIT_SYSTEM="$(ps --no-headers -o comm 1)"
    case "$INIT_SYSTEM" in
        "systemd")
            install_systemd_service
            ;;
        "init")
            install_initd_service
            ;;
        *)
            echo "Don't know how to install on init system '$INIT_SYSTEM'" >&2
            exit 1
            ;;
    esac
}

main