/**
 * @file    pss.h
 * @author  Randall Pittman (pittmara@oregonstate.edu)
 * @brief   Acquire the persistent set size (PSS) in kilobytes of any process.
 * @version 0.1
 * @date    2024-12-19
 *
 * If `get_pss()` returns 1
 *
 * If a program runs `get_pss()` with different uid and euid (setuid functionality), it
 * must first call `setup_suid_guard()`. The setuid guard turns off suid functionality
 * (euid is set to uid) until the program is in the section that needs it, temporarily
 * enables it, and then disables it when it is no longer needed. Call `disable_suid()`
 * when setuid functionality is no longer needed.
 *
 * @copyright Copyright (c) 2024
 *
 */

#ifndef PSS_H
#define PSS_H
#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>  // pid_t

// Values for pss_errno
typedef enum pss_err {
    PSS_ENOERR = 0,    /* no error*/
    PSS_ESUIDGUARD,    /* suid guard not set up*/
    PSS_ESMAPSACC,     /* smaps access disallowed */
    PSS_ENOSMAPS,      /* smaps file missing */
    PSS_EPIDOVERFLOW,  /* smaps pid string too long */
    PSS_ENOPSSINFO,    /* No PSS info is contained in the smaps file */
    PSS_ESUID_ERR,     /* suid_errno saved, string from suid_strerr will be used */
    PSS_ERRNO,         /* errno saved, string will be returned by pss_errstr and used in pss_perror */
} pss_err_t;


/// @brief Like errno but for get_pss().
extern pss_err_t pss_errno;

/**
 * @brief Get an error string for the error in `pss_errno`;
 *
 * @return const char*  The error string.
 */
const char* pss_strerr();

/**
 * @brief Print out the error message for `pss_errno`.
 *
 * --Duplicated from perror--
 * Prints a line on stderr consisting of the text in `msg`, a colon, a space, a message
 * describing the meaning of the contents of `pss_errno` and a newline. If `msg` is NULL
 * or "", the colon and space are omitted.
 *
 * @param msg  Text to prefix error message.
 */
void pss_perror(const char* msg);

/**
 * @brief Get the total PSS (persistent set size, in kB) of a process from
 * /proc/$pid/smaps
 *
 * @param[in]  pid The pid of the process
 * @param[out] pss The total PSS of the process, in kiB
 * @return int  0: success. 1: error
 *
 * `pss_errno` may be set to any of the PSS_E* values. Get use pss_strerr to get a string
 * or pss_perror to print the error.
 */
int get_pss(pid_t pid, unsigned long* pss_kiB);

#ifdef __cplusplus
}
#endif
#endif  /* PSS_H */
