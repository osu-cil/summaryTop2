#include <errno.h>      // errno, ERANGE
#include <fcntl.h>      // open, fdopen, etc.
#include <sys/types.h>  // pid_t
#include <stdbool.h>
#include <stdio.h>      // print fns, FILE, stderr
#include <string.h>     // strlen
#include <unistd.h>     // getuid, geteuid, seteuid

#include "suid_guard.h"
#include "pss.h"

// pulled out from a version of fcntl for compatibility
#ifndef O_NOFOLLOW
#define O_NOFOLLOW	00400000	/* don't follow links */
#endif

#define PSS_BUFFER_SIZE 1024  // standard string buffer

pss_err_t pss_errno = PSS_ENOERR;
/// @brief errno value saved for use in pss_strerr
unsigned int pss_saved_errno = 0;
/// @brief suid_errno saved for use in pss_strerr
suid_err_t suid_saved_errno = SUID_NOERROR;

/// @brief String representations of pss_err
const char* const _pss_errs[] = {
    "Success",  /* PSS_ENOERR */
    "setup_suid_guard() must be run before running get_pss() with a different euid (suid)", /* PSS_ESUIDGUARD */
    "Access to the smaps file is not allowed",  /* PSS_ESMAPSACC */
    "The smaps file does not exist",  /* PSS_ENOSMAPS */
    "The pid is too long", /* PSS_EPIDOVERFLOW */
    "No PSS info contained in the smaps file", /* PSS_ENOPSSINFO */
    "",  /* PSS_ESUID_ERR */ // suid_strerror returned instead
    "",  /* PSS_ERRNO */ // strerror returned instead
};

/**
 * @brief Save errno for later parsing and set pss_errno to indicate we've done this.
 *
 */
void save_errno() {
    pss_saved_errno = errno;
    pss_errno = PSS_ERRNO;
}

/**
 * @brief Save suid_errno for later parsing and set pss_errno to indicate we've done this.
 *
 */
void save_suid_errno() {
    suid_saved_errno = suid_errno;
    pss_errno = PSS_ESUID_ERR;
}

const char* pss_strerr(pss_err_t errnum) {
    size_t _pss_errs_len = sizeof(_pss_errs) / sizeof(_pss_errs[0]);
    if (errnum < _pss_errs_len) {
        if (errnum == PSS_ESUID_ERR) return suid_strerr(suid_saved_errno);
        if (errnum == PSS_ERRNO) return strerror(pss_saved_errno);
        else return _pss_errs[errnum];
    }
    else return "Unknown error";
}

void pss_perror(const char* msg) {
    if (strlen(msg) > 0){
        fprintf(stderr, "%s: %s\n", msg, pss_strerr(pss_errno));
    } else {
        fprintf(stderr, "%s\n", pss_strerr(pss_errno));
    }
}

/**
 * @brief Helper function to create a path to a file in /proc/$pid/...
 *
 * @param pid           PID of the process
 * @param file_name     proc filename
 * @param buffer        Buffer into which the path is written
 * @param buffer_size   Size of buffer
 * @return int          0: success. 1: failure (pss_errno is set).
 */
int construct_proc_pid_path(
    pid_t pid, const char* file_name, char* buffer, size_t buffer_size
) {
    if (snprintf(buffer, buffer_size, "/proc/%d/%s", pid, file_name) >= buffer_size) {
        pss_errno = PSS_EPIDOVERFLOW;
        return 1;
    }
    return 0;
}

/**
 * @brief Read-only fopen with O_NOFOLLOW flag to ensure symlinks are not followed.
 *
 * @param smaps_pathname  Path to the file to open.
 * @return                Pointer to the open file stream, or NULL if there's an error.
 *
 * Possible `pss_errno` values: PSS_ESMAPSACC, PSS_ENOSMAPS, PSS_ECLEARENV, PSS_ESETEUID,
 * PSS_ERRNO
 */
FILE* fopen_smaps_file(const char *restrict smaps_pathname) {
    // We use O_NOFOLLOW to avoid symbolic link attacks
    errno = 0;
    int fd = open(smaps_pathname, O_RDONLY | O_NOFOLLOW);
    if (fd < 0) {  // some error occurred
        if (errno == EACCES) pss_errno = PSS_ESMAPSACC;
        else if (errno == ENOENT) pss_errno = PSS_ENOSMAPS;
        else save_errno();  // unexpected errors
        return NULL;
    }
    FILE* stream = fdopen(fd, "r");
    if (stream == NULL) {  // some error occurred
        close(fd);
        save_errno();  // should already be open, so this is for unlikely errors.
        return NULL;
    }
    return stream;
}

/**
 * @brief Open a file given a path to the file, possibly with setuid.
 *
 * If the file cannot be opened, try getting suid (restore the euid) and try again. If
 * this succeeds, try to release the suid guard immediately (set euid to uid). If that
 * fails for some reason, immediately close the file and signal an error.
 *
 * @param smaps_path  Path to the file
 * @param smaps_file  FILE pointer to the opened file
 * @return int        0: success. 1: failure (see pss_errno).
 */
int fopen_smaps_maybe_suid(const char* smaps_path, FILE** smaps_file) {
    FILE* file = fopen_smaps_file(smaps_path);  // First, try without setuid
    if (!file) {
        // Ok, try with setuid
        errno = 0;  // reset
        if (acquire_suid_guard() != 0) {
            save_suid_errno();
            return 1;
        }
        file = fopen_smaps_file(smaps_path);
        if (!file) return 1;
        if (release_suid_guard() != 0) {
            save_suid_errno();
            fclose(file);
            return 1;
        }
    }
    *smaps_file = file;
    return 0;
}

/**
 * @brief Get the persistent set size from a /proc/$pid/smaps or /proc/$pid/smaps_rollup
 *        file.
 *
 * The values in any lines starting with 'Pss:', 'Private_Hugetlb:', or 'Shared_Hugetlb:'
 * are summed, plus 500 are added.
 *
 * @param smaps_file
 * @param pss_sum_bytes
 * @return true
 * @return false
 */
bool pss_from_smaps_file(FILE* smaps_file, unsigned long* pss_sum_bytes) {
    bool got_pss = false;
    char line[PSS_BUFFER_SIZE] = "";
    while (fgets(line, PSS_BUFFER_SIZE, smaps_file)) {
        unsigned long int value;
        // NOTE: I don't remember where I got this pseudo-definition of PSS (including
        // the Hugetlb entries and adding 0.5 kiB), but here we are. It's not too far off
        // from just getting PSS.
        if (
            sscanf(line, "Pss: %lu kB", &value) == 1 ||
            sscanf(line, "Private_Hugetlb: %lu kB", &value) == 1 ||
            sscanf(line, "Shared_Hugetlb: %lu kB", &value) == 1
        ) {
            *pss_sum_bytes += (1024lu * value) + 512lu;  // Add the PSS value
            got_pss = true;
        }
    }
    return got_pss;
}

int get_pss(pid_t pid, unsigned long* pss_kiB) {
    // ensure setup_suid_guard() has been run before doing anything else.
    if (!check_suid_guard()) {
        pss_errno = PSS_ESUIDGUARD;
        return 1;
    }
    char smaps_path[PSS_BUFFER_SIZE] = "";
    FILE* smaps_file = NULL;
    bool got_pss = false;
    unsigned long pss_sum_bytes = 0;
    // Try smaps_rollup first
    if (construct_proc_pid_path(pid, "smaps_rollup", smaps_path, PSS_BUFFER_SIZE) != 0) return 1;
    if (fopen_smaps_maybe_suid(smaps_path, &smaps_file) != 0) {
        if (pss_errno != PSS_ENOSMAPS) return 1;  // Some error other than smaps_rollups doesn't exist
        // smaps_rollup is missing, try smaps
        if (construct_proc_pid_path(pid, "smaps", smaps_path, PSS_BUFFER_SIZE) != 0) return 1;
        if (fopen_smaps_maybe_suid(smaps_path, &smaps_file) != 0) return 1;
    }
    got_pss = pss_from_smaps_file(smaps_file, &pss_sum_bytes);
    fclose(smaps_file);
    if (!got_pss) {
        pss_errno = PSS_ENOPSSINFO;
        return 1;
    }
    *pss_kiB = pss_sum_bytes / 1024;  // Return kB
    return 0;
}
