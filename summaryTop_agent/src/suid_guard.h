/**
 * @file    suid_guard.h
 * @author  Randall Pittman (pittmara@oregonstate.edu)
 * @brief   Helper functions for modifying the effective uid of a process.
 * @version 0.1
 * @date    2024-12-30
 *
 * @copyright Copyright (c) 2024
 *
 */

#ifndef SUID_GUARD_H
#define SUID_GUARD_H
#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>  // bool

/// @brief Possible error codes for suid_guard
typedef enum suid_err {
    SUID_NOERROR = 0,
    SUID_ECLEARENV,
    SUID_ESETEUID,
    SUID_ESETUID,
} suid_err_t;

/// @brief Like errno but for setuid_guard.
extern suid_err_t suid_errno;

/**
 * @brief Get an error string for the error in `suid_errno`;
 *
 * @return const char*  The error string.
 */
const char* suid_strerr(suid_err_t errnum);

/**
 * @brief Print out the error message for `suid_errno`.
 *
 * --Duplicated from perror--
 * Prints a line on stderr consisting of the text in `msg`, a colon, a space, a message
 * describing the meaning of the contents of `suid_errno` and a newline. If `msg` is NULL
 * or "", the colon and space are omitted.
 *
 * @param msg  Text to prefix error message.
 */
void suid_perror(const char* msg);

/**
 * @brief Check if the suid guard needs to be set up.
 *
 * @return true     The uid and euid are the same or they are different and the guard has
 *                  been set up.
 * @return false    The uid and euid are different and the guard has not been set up.
 */
bool check_suid_guard();

/**
 * @brief Save the real and effective uid and turn off setuid functionality.
 *
 * Use suid_strerr to get error string or suid_perror to print error.
 *
 * @return int  0: success, 1: error
 *
 * Possible `suid_errno` values: SUID_ECLEARENV, SUID_ESETUID
*/
int setup_suid_guard();

/**
 * @brief Restore the current process's effective uid (turn on setuid functionality).
 *
 * @return int  0: success, 1: error
 *
 * Possible `suid_errno` values: SUID_ECLEARENV, SUID_ESETUID
 */
int acquire_suid_guard();

/**
 * @brief Temporarily turn off setuid functionality.
 *
 * @return int  0: success, 1: error
 *
 * Possible `suid_errno` values: SUID_ECLEARENV, SUID_ESETUID
 */
int release_suid_guard();

/**
 * @brief Permanently turn off setuid functionality for the duration of the program.
 *
 * @return int  0: success, 1: error
 *
 * Possible `suid_errno` values: SUID_ECLEARENV, SUID_ESETUID
 */
int disable_suid();

#ifdef __cplusplus
}
#endif
#endif  /* SUID_GUARD_H */
