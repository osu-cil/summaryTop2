/**
 * user_procsums.c - Create a user-by-user list of summed process statistics.
 *
 * Most info is acquired from running and parsing the `ps` command. However, on many (even
 * fairly recent) versions of ps, PSS (proportional set size) is not available. Therefore,
 * this program sums the the Pss: values from /proc/$pid/smaps for each process.
 */

// Make sure we get DT_DIR
#define _BSD_SOURCE       // glibc < 2.19
#define _DEFAULT_SOURCE   // glibc 2.19+
#include <argp.h>      // argp_parse, argp_program_version, argp_option, argp_state
#include <ctype.h>     // isdigit
#include <dirent.h>    // dirent, opendir, readdir
#include <pwd.h>
#include <stdbool.h>   // bool, true, false
#include <stdio.h>     // fopen, sscanf, fgets, pclose
#include <stdlib.h>    // strtol
#include <string.h>    // strrchr. strncpy
#include <sys/stat.h>  // stat
#include <unistd.h>    // getpid, sysconf

#include "pss.h"
#include "suid_guard.h"

#define BUFFER_SIZE 1024
#define INITIAL_USER_COUNT 128
#define MAX_NPIDS_TO_SKIP 256

/**
 * @brief Structure to hold user data (uid, username, utime, stime, RSS, PSS, and total
 *        process count for all processes of the user.)
 *
 */
struct user_info {
    int uid;
    char user[BUFFER_SIZE];
    unsigned long utime;
    unsigned long stime;
    unsigned long rss_kiB;
    unsigned long pss_kiB;
    int process_count;
};

int get_username(uid_t uid, char* buf, int buf_len) {
    struct passwd* pw;
    pw = getpwuid(uid);
    strncpy(buf, pw->pw_name, buf_len - 1);
    return 0;
}

/**
 * @brief Dynamically store and retrieve user data by UID
 *
 * @param users             Dynamic array of structures of user information
 * @param user_count        Number of populated entries in `users`
 * @param allocated_count   Number of entries in `users`, included unpopulated entries
 * @param uid               uid to find in `users`, or if not found to add as a new entry.
 * @return int              Index of the entry in `users` found or created.
 */
int find_or_add_user(
    struct user_info* users,
    int* user_count,
    int* allocated_count,
    int uid
) {
    for (int i = 0; i < *user_count; i++) {
        if (users[i].uid == uid) {
            return i;  // User already exists
        }
    }

    // If user does not exist, add them to the array
    // Extend the array of users, if neccessary.
    if (*user_count >= *allocated_count) {
        *allocated_count *= 1.25;  // 25% larger
        users = realloc(users, *allocated_count * sizeof(struct user_info));
        if (!users) {
            perror("Failed to allocate memory for users");
            exit(EXIT_FAILURE);
        }
    }

    users[*user_count].uid = uid;
    get_username(uid, users[*user_count].user, sizeof(users[*user_count].user));
    users[*user_count].user[sizeof(users[*user_count].user) - 1] = '\0';
    users[*user_count].utime = 0ull;
    users[*user_count].stime = 0ull;
    users[*user_count].rss_kiB = 0ll;
    users[*user_count].pss_kiB = 0ll;
    users[*user_count].process_count = 0;

    return (*user_count)++;
}

/// @brief Check that a directory entry is a directory with a fully numeric name
static bool is_pid_dir(const struct dirent* entry) {
    const char* p;
    if (entry->d_type != DT_DIR) return false;
    for (p = entry->d_name; *p; p++) {
        if (!isdigit(*p)) return false;
    }
    return true;
}

/**
 * @brief Get the owner uid of a path in the filesystem.
 *
 * @param[in] path Filesystem path
 * @param[out] uid uid of the path
 * @return int 0: success 1:failure of stat() call (see errno)
 */
static int path_uid(const char* restrict path, uid_t* uid) {
    struct stat sb;
    if (stat(path, &sb) != 0) return 1;
    *uid = sb.st_uid;
    return 0;
}

/**
 * @brief Try to read the entirety of a file into a fixed-size buffer.
 *
 * @param[in]  path        Path to the file to read in.
 * @param[out] outbuf      Buffer in which to store the contents of the file.
 * @param[in]  outbuf_size Size of the output buffer.
 * @return int
 */
int readin_file(const char* path, char* restrict outbuf, int outbuf_size) {
    // Open and read the stat file into pid_stat_line
    FILE* fp = fopen(path, "r");
    if (fp == NULL) return 1;
    if (fgets(outbuf, outbuf_size, fp) == NULL) return 1;
    fclose(fp);
    return 0;
}

/**
 * @brief  Read text from a /proc/$pid/stat file, extracting just utime, stime, and rss.
 *
 * @param[in] S           Text from the /proc/$pid/stat file
 * @param[out] utime      User time
 * @param[out] stime      System time of process
 * @param[out] rss_pages  RSS in pages
 * @return int
 */
static int stat2vals (
    const char* S,
    unsigned long* utime,
    unsigned long* stime,
    unsigned long* rss_pages
) {
    // N.B. - This is simplified from procps/library/readproc.c:stat2proc()
    // Get past the process name
    S = strrchr(S, ')');  // note 'reverse' - last closing parenthesis in string
    S += 2;               // skip ") "
    sscanf(
        S,
       "%*c "                      // state
       "%*d %*d %*d %*d %*d "      // ppid, pgrp, sid, tty_nr, tty_pgrp
       "%*lu %*lu %*lu %*lu %*lu " // flags, min_flt, cmin_flt, maj_flt, cmaj_flt
       "%lu %lu %*lu %*lu "        // utime, stime, cutime, cstime
       "%*d %*d "                  // priority, nice
       "%*d "                      // num_threads
       "%*lu "                     // 'alarm' == it_real_value (obsolete, always 0)
       "%*llu "                    // start_time
       "%*lu "                     // vsize
       "%lu ",                     // rss
       // omit further fields
       utime, stime, rss_pages
    );
    return 0;
}

/// @brief Arguments struct for user_procsums.
struct args_struct {
    pid_t pids_to_skip[MAX_NPIDS_TO_SKIP];
    bool exclude_self;
    bool header;
};

/**
 * @brief Determine if a PID should be skipped based on the exclude_self and pids_to_skip
 * arguments.
 *
 * @param pid       The PID to check
 * @param arguments The program arguments
 * @return true     Skip this PID.
 * @return false    Do not skip this PID (get its info).
 */
bool skip_pid(pid_t pid, const struct args_struct* arguments) {
    if (arguments->exclude_self && pid == getpid()) return true;
    // Note that pids_to_skip is preinitialized with -1s so the first -1 acts as a
    // sentinel value.
    for (int i = 0; arguments->pids_to_skip[i] != -1; i++) {
        if (pid == arguments->pids_to_skip[i]) return true;
    }
    return false;
}

/**
 * @brief Run the ps command, process the output, and print the summed statistics.
 *
 * @param print_header  If TRUE (1), print a row of headers when printing the stats.
 */
int ps(const struct args_struct* arguments) {
    // Dynamically allocate memory for user info
    int allocated_count = INITIAL_USER_COUNT;
    struct user_info* users = malloc(allocated_count * sizeof(struct user_info));
    if (!users) {
        perror("Failed to allocate memory for users");
        return 1;
    }
    int user_count = 0;  // Number of unique users

    DIR* procdir;
    struct dirent* entry;
    char pid_stat_path[256 + 5 + 5 + 1] = "";  // dirent.d_name + /proc + /stat + /0
    char pid_stat_line[BUFFER_SIZE] = "";
    unsigned long utime = 0ul, stime = 0ul, rss_pages = 0ul, pss_kiB = 0ul;
    long page_size = sysconf(_SC_PAGESIZE);
    pid_t pid;
    uid_t uid;

    if ((procdir = opendir("/proc")) == NULL) return 1;
    while ((entry = readdir(procdir))) {
        if (!is_pid_dir(entry)) continue;  // ensures dirname is numeric
        snprintf(pid_stat_path, sizeof(pid_stat_path), "/proc/%s/stat", entry->d_name);
        pid = (pid_t)strtol(entry->d_name, NULL, 10);  // We already know its numeric.
        if (skip_pid(pid, arguments)) continue;
        // Get the uid of the process (uid of the stat file)
        if (path_uid(pid_stat_path, &uid) != 0) continue;  // path has disappeared? Move on.
        if (readin_file(pid_stat_path, pid_stat_line, sizeof(pid_stat_line)) != 0) {
            // File has disappeared? Move on.
            continue;
        }
        if (stat2vals(pid_stat_line, &utime, &stime, &rss_pages) != 0) {
            fprintf(stderr, "pid_stat_path: %s\n", pid_stat_path);
            perror("Failed to parse values from a /proc/$pid/stat file");
            return 1;
        }
        if (get_pss(pid, &pss_kiB) != 0) {
            if (pss_errno != PSS_ENOSMAPS && pss_errno != PSS_ENOPSSINFO) {
                pss_perror("user_procsums get_pss error");
                return 1;
            }
            pss_kiB = 0.0;
        };

        // Find or add user by UID
        int index = find_or_add_user(users, &user_count, &allocated_count, uid);
        users[index].utime += utime;
        users[index].stime += stime;
        users[index].rss_kiB += rss_pages * page_size / 1024;
        users[index].pss_kiB += pss_kiB;
        users[index].process_count++;
    }
    if (disable_suid() != 0) pss_perror("user_procsums error disabling suid");

    // Output the aggregated data
    if (arguments->header) {
        printf(
            "%8s\t%20s\t%5s\t%11s\t%11s\t%9s\t%9s\n",
            "UID", "USER", "NPROC", "UTIME", "STIME", "RSS", "PSS"
        );
    }
    for (int i = 0; i < user_count; i++) {
        printf(
            "%8d\t%20s\t%5d\t%11llu\t%11llu\t%9lld\t%9lld\n",
            users[i].uid, users[i].user, users[i].process_count, users[i].utime,
            users[i].stime, users[i].rss_kiB, users[i].pss_kiB
        );
    }
    // Cleanup
    free(users);
    return 0;
}

/// @brief Output with --version flag
const char* argp_program_version = "user_procsums 0.1";

/// @brief Specification of command-line options for argp.
static struct argp_option options[] = {
    // long_name, (char/int) key, arg name (if any), option flags, docstring, option group
    {
        "pids_to_skip", 'p', "PIDS_TO_SKIP", 0,
        (
            "Comma-separated list of pids to exclude from calculations (max 256)."
        ),
        0
    },
    {"exclude_self", 's', 0, 0, "Exclude user_procsums process from calculations.", 0},
    {"header", 'h', 0, 0, "Print headers above each column.", 0},
    {0},  // sentinel required by argp
};

/// @brief Help text for user_procsums
static char doc[] = (
    "user_procsums -- prints the sum of %CPU , RSS, and PSS (proportional set size)\n"
    "for each user in the system.\n"
    "\v"  // vertical tab - below text will go after options doc
    "Columns are uid, user name, total processes per user, summed %cpu, summed RSS,\n"
    "and summed PSS.\n"
);

/**
 * @brief Parse a string like "123,456,789" into an array of PIDs, up to MAX_NPIDS_TO_SKIP
 *
 * @param[in]  pids_arg  Comma-separate string of pids
 * @param[out] pid_arr   PIDs extracted from pids_arg
 * @return int           Number of pids in pid_arr, or -1 if there's an error.
 */
int parse_pids(const char* pids_arg, pid_t* pid_arr) {
    if (!pids_arg || !pid_arr) return -1;  // Invalid input

    int npids = 0;
    const char* ptr = pids_arg;
    char* endptr;

    while (*ptr != '\0' && npids < MAX_NPIDS_TO_SKIP) {
        // read from ptr up to first non-digit
        long val = strtol(ptr, &endptr, 10);
        if (ptr == endptr) {  // strtol didn't find a number at the start of ptr
            fprintf(stderr, "Invalid input: %s\n", ptr);
            return -1;
        }
        if (val < 0 || val > INT_MAX) {  // not a pid
            fprintf(stderr, "Value out of range: %ld\n", val);
            return -1;
        }
        // PID is OK, save and move to the next number
        pid_arr[npids++] = (int)val;
        // Advance the pointer, skipping the comma if present.
        ptr = (*endptr == ',') ? endptr + 1 : endptr;
    }
    return npids;  // Return the number of parsed values
}

/// @brief Parser function for argp parser
error_t arg_parser(int key, char* arg, struct argp_state* state) {
    if (!state) return ARGP_ERR_UNKNOWN;
    struct args_struct* arguments = state->input;
    switch (key) {
        case 'p':
            if (!arg) return EINVAL;
            if (parse_pids(arg, arguments->pids_to_skip) <= 0) {
                fprintf(stderr, "'%s' is not valid for PIDS_TO_SKIP.\n", arg);
                return EINVAL;
            }
            break;
        case 's':
            arguments->exclude_self = true;
            break;
        case 'h':
            arguments->header = true;
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

/// @brief The argp parser
static struct argp argp = {options, arg_parser, 0, doc};  // options, parser, args_doc, doc

/**
 * @brief Entry point for user_procsums. Print the usage info if requested, ensure the
 * process is running as root, and call `ps` to get the process info.
 *
 * @param argc Number of command-line arguments.
 * @param argv Command-line arguments.
 * @return int 0 on success, 1 if run as non-root user.
 */
int main(int argc, char* argv[]) {
    // Root/suid checks
    if (geteuid() != 0) {
        fprintf(
            stderr,
            "This program must be run as root or owned by root with setuid enabled to be"
            " able to read PSS data from smaps files for all users.\n"
        );
        return 1;
    }
    if (setup_suid_guard() != 0) {
        pss_perror("get_pss error setting up suid guard");
        return 1;
    };
    // Set up default arguments
    struct args_struct arguments = {
        .pids_to_skip = {[0 ... 255] = -1},
        .exclude_self = false,
        .header = false
    };
    // Parse arguments
    int argp_ret = argp_parse(&argp, argc, argv, 0, 0, &arguments);
    if (argp_ret != 0) return argp_ret;
    // Run the ps function
    return ps(&arguments);
}
