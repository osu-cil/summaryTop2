#include <errno.h>      // errno, ERANGE
#include <limits.h>     // LONG_MAX, INT_MAX
#include <math.h>       // log10
#include <stdio.h>      // print fns, stderr
#include <stdlib.h>     // strtol
#include <string.h>     // strlen
#include <unistd.h>

#include "pss.h"
#include "suid_guard.h"

/**
 * @brief Parse an string into a long and only return if parsing was successful.
 *
 * @param str       The string to parse.
 * @param result    The parsed long value.
 * @return int      0: success, -1: overflow, -2: underflow, -3: no digits were found,
 *                  -4 extra (non-numeric) characters were present in the string.
 */
int safe_parse_long(const char *str, long *result) {
    char *endptr;
    errno = 0;  // Clear errno before calling strtol
    long value = strtol(str, &endptr, 10);
    // Check for parsing errors
    if (errno == ERANGE) {
        if (value == LONG_MAX) return -1;
        else return -2;
    }
    if (endptr == str) return -3;
    if (*endptr != '\0') return -4;
    *result = value;
    return 0;
}

/**
 * @brief Parse a string into a pid
 *
 * @param str   The string to parse.
 * @param pid   The output pid.
 * @return int  0: success, -1: overflow, -2: underflow, -3: no
 *              digits were found, -4 extra (non-numeric) characters were present in the
 *              string.
 */
int pid_from_str(const char* str, pid_t *pid) {
    long value;
    int ret = safe_parse_long(str, &value);
    if (ret != 0) return ret;
    if (value < 0) return -2;
    if (value > INT_MAX) return -1;
    *pid = (pid_t) value;
    return 0;
}

/**
 * @brief Read a PID from the command line and return the proportional set size of the
 *        process represented by that pid.
 *
 * @param argc  Number of command-line arguments.
 * @param argv  Command-line arguments.
 * @return int  0: success, 1: An incorrect number of arguments were provided. 2: The
 *              provided argument was not a valid integer to use as a pid. 3: No process
 *              exists with the supplied pid. 4: There was an error getting the PSS.
 */
int main(int argc, char* argv[]) {
    if (setup_suid_guard() != 0) {
        pss_perror("get_pss error setting up suid guard");
        return 1;
    };
    // Check and parse the input
    if (argc != 2) {
        fprintf(stderr, "One and only one argument must be supplied: a PID.\n");
        return 1;
    }
    pid_t pid = -1;
    size_t arglen = strlen(argv[1]);
    if (arglen < 1 || arglen > 10 || pid_from_str(argv[1], &pid) != 0) {
        fprintf(stderr, "get_pss error: Invalid PID argument: %s\n", argv[1]);
        return 2;
    }
    if (getpgid(pid) < 0) {
        fprintf(stderr, "get_pss error: No process exists with pid %d.\n", pid);
        return 3;
    }
    // Get the PSS
    unsigned long pss = 0;
    if (get_pss(pid, &pss) != 0) {
        pss_perror("get_pss error: Error getting pss");
        return 4;
    }
    printf("%lu\n", pss);
    return 0;
}
