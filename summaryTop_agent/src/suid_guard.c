#include <stdbool.h>    // bool
#include <stdio.h>      // fprintf, stderr
#include <stdlib.h>     // clearenv
#include <string.h>     // strlen
#include <sys/types.h>  // uid_t
#include <unistd.h>     // getuid, geteuid, seteuid

#include "suid_guard.h"

suid_err_t suid_errno = SUID_NOERROR;
uid_t uid;
uid_t orig_euid;
bool _suid_guard_set_up = false;

const char* const _suid_errs[] = {
    "Success",              // SUID_NOERROR
    "clearenv call failed", // SUID_ECLEARENV
    "seteuid call failed",  // SUID_ESETEUID
    "setuid call failed",   // SUID_ESETUID
};

const char* suid_strerr(suid_err_t errnum) {
    size_t _suid_errs_len = sizeof(_suid_errs) / sizeof(_suid_errs[0]);
    if (errnum < _suid_errs_len) return _suid_errs[errnum];
    else return "Unknown error";
}

void suid_perror(const char* msg) {
    if (strlen(msg) > 0){
        fprintf(stderr, "%s: %s\n", msg, suid_strerr(suid_errno));
    } else {
        fprintf(stderr, "%s\n", suid_strerr(suid_errno));
    }
}

bool check_suid_guard() {
    return getuid() == geteuid() || _suid_guard_set_up;
}

int setup_suid_guard() {
    if (clearenv() != 0) {suid_errno = SUID_ECLEARENV; return 1;}
    uid = getuid();
    orig_euid = geteuid();
    int ret = release_suid_guard();
    if (ret != 0) return ret;
    _suid_guard_set_up = true;
    return 0;
}

int acquire_suid_guard(){
    // Sanitize environment first
    if (clearenv() != 0)            {suid_errno = SUID_ECLEARENV; return 1;}
    if (seteuid(orig_euid) != 0)    {suid_errno = SUID_ESETEUID;  return 1;}
    return 0;
}

int release_suid_guard(){
    if (clearenv() != 0)    {suid_errno = SUID_ECLEARENV; return 1;}
    if (seteuid(uid) != 0)  {suid_errno = SUID_ESETEUID;  return 1;}
    return 0;
}

int disable_suid(){
    if (clearenv() != 0)    {suid_errno = SUID_ECLEARENV; return 1;}
    if (setuid(uid) != 0)   {suid_errno = SUID_ESETUID;   return 1;}
    return 0;
}
