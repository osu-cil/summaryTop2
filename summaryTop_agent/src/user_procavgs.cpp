/**
 * @file user_procavgs.cpp
 * @author Randall Pittman
 * @brief Program to spit out user averages of cpu and memory usage.
 * @version 0.1
 * @date 2025-01-08
 *
 * @copyright Copyright (c) 2025
 *
 */
// Make sure we get DT_DIR (prefer compile flags?)
#define _BSD_SOURCE       // glibc < 2.19
#define _DEFAULT_SOURCE   // glibc 2.19+
#include <cctype>  // isdigit
#include <chrono>
#include <cstdio>  // fprintf, perror, printf
#include <cstdlib>  // strtol
#include <fstream>  //
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
extern "C" {
    #include <dirent.h>  // dirent, opendir, readdir
    #include <sys/stat.h>
    #include <unistd.h>  // sleep, geteuid, pid_t
}
#include "pss.h"
#include "suid_guard.h"

// <chrono>
using Clock = std::chrono::steady_clock;
using TimePoint = std::chrono::time_point<Clock>;
// <cstdio>
using std::printf;
using std::fprintf;
// <cstdlib>
using std::strtol;
// <memory>
using std::unique_ptr;
// <string>
using std::string;
using std::to_string;
// <vector>
using std::vector;


/// @brief Check that a directory entry is a directory with a fully numeric name
static bool is_pid_dir(const struct dirent * entry) {
    const char* p;
    if (entry->d_type != DT_DIR) return false;
    for (p = entry->d_name; *p; p++) {
        if (!std::isdigit(*p)) return false;
    }
    return true;
}

/**
 * @brief Determine if a PID should be skipped based on the exclude_self and pids_to_skip
 * arguments.
 *
 * @param pid       The PID to check
 * @param arguments The program arguments
 * @return true     Skip this PID.
 * @return false    Do not skip this PID (get its info).
 */
static bool skip_pid(pid_t pid, bool exclude_self, const vector<pid_t>& pids_to_skip) {
    if (exclude_self && pid == getpid()) return true;
    for (const pid_t & skip_pid : pids_to_skip) {
        if (pid == skip_pid) return true;
    }
    return false;
}

/**
 * @brief Get the owner uid of a path in the filesystem.
 *
 * @param[in] path Filesystem path
 * @param[out] uid uid of the path
 * @return int 0: success 1:failure of stat() call (see errno)
 */
static int get_path_uid(const string& path, uid_t& uid) {
    struct stat sb;
    if (stat(path.c_str(), &sb) != 0) return 1;
    uid = sb.st_uid;
    return 0;
}

struct PsResult {
    TimePoint ps_time;
    unsigned long utime;
    unsigned long stime;
    unsigned long rss_pages;
};

/**
 * @brief  Read utime, stime, and rss (in pages) from /proc/[pid]/stat and pss from
 * /proc/<pid>/smaps[_rollup].
 *
 * Note that root is required to read the smaps[_rollup] file of other users.
 *
 * @param[in]  pid_stat_path  Path to the stat file
 * @param[out] uid            UID of the owning user of the process
 * @param[out] result         Data extracted from the stat and smaps[_rollup] files
 * @return int
 */
int ps(
    pid_t pid,
    TimePoint& ps_time,
    uid_t& uid,
    unsigned long& utime,
    unsigned long& stime,
    unsigned long& rss_kiB,
    unsigned long& pss_kiB
) {
    ps_time = Clock::now();
    unsigned long rss_pages;
    string pid_stat_path = "/proc/"  + to_string(pid) + "/stat";
    // Get the uid of the process (uid of the stat file)
    if (get_path_uid(pid_stat_path, uid) != 0) return -1;  // path has disappeared? Move on.
    std::ifstream pid_stat_stream(pid_stat_path);
    std::string line;  // holds the whole line
    std::string tmp;  // holds values to skip
    if (!pid_stat_stream.is_open())
        return -1;  // couldn't open the file
    if (!std::getline(pid_stat_stream, line))
        return -1;  // Couldn't read in the line
    std::size_t pos = line.rfind(')');  // last closing paren
    if (pos == std::string::npos || pos + 2 >= line.size())
        return -1;  // paren not found or nothing after it
    std::istringstream iss(line.substr(pos + 2));
    for (int i = 0; i < 11; i++)
        // skip 11 fields: state, ppid, pgrp, session, tty_nr, tpgid, flags, minflt,
        // cminflt, majflt, cmajflt
        if (!(iss >> tmp))
            return -2;  // Failed to read a value
    if (!(iss >> utime >> stime))
        return -2;  // Failed to read utime or stime
    for (int i = 0; i < 8; i++)
        // skip 8 fields: cutime, cstime, priority, nice, num_threads, itrealvalue,
        // starttime, vsize
        if (!(iss >> tmp))
            return -2;  // Failed to read a value
    if (!(iss >> rss_pages))
        return -2;  // failed to read rss_pages
    static const long page_size = sysconf(_SC_PAGESIZE);
    rss_kiB = rss_pages * page_size / 1024;
    if (get_pss(pid, &pss_kiB) != 0)
        return -2;  // Failed to get pss
    return 0;
}

int ps_all(bool exclude_self, const vector<pid_t>& pids_to_skip) {
    DIR* procdir = nullptr;
    struct dirent* entry = nullptr;
    pid_t pid = -1;
    uid_t uid;
    TimePoint t;
    unsigned long utime, stime, rss_kiB, pss_kiB;
    if ((procdir = opendir("/proc")) == NULL) return 1;
    while ((entry = readdir(procdir))) {
        if (!is_pid_dir(entry)) continue;
        pid = (pid_t)strtol(entry->d_name, NULL, 10);  // We already know it's numeric.
        if (skip_pid(pid, exclude_self, pids_to_skip)) continue;
        if (ps(pid, t, uid, utime, stime, rss_kiB, pss_kiB) != 0) continue;
        // In here we would call ps_pid to get the utime, stime, rss, and pss of the
        // process
        printf("%10d: %7d %11lu %11lu %11lu %11lld\n", pid, uid, utime, stime, rss_kiB, pss_kiB);
    }
    return 0;
}

/**
 * @brief Parse a comma-separated list of integers into a vector of PIDs
 *
 * @param pids_arg               commas-separated string of integers
 * @return std::vector<pid_t>    - vector of PIDs
 *
 * @throws std::invalid_argument - stoi couldn't parse a string to an int
 * @throws std::out_of_range     - stoi got a number too big to be an int
 */
vector<pid_t> parse_pids(const char* pids_arg) {
    vector<pid_t> result;
    std::stringstream ss(pids_arg);
    std::string item;
    while (std::getline(ss, item, ',')) {
        result.push_back((pid_t)std::stoi(item));
    }
    return result;
}

int main(int argc, char * argv[]) {
    // hardcoded parameters for now
    int ps_interval_ms = 5000;
    if (geteuid() != 0) {
        fprintf(
            stderr,
            "This program must be run as root or owned by root with setuid enabled to be"
            " able to read PSS data from smaps files for all users.\n"
        );
        return 1;
    }
    if (setup_suid_guard() != 0) {
        suid_perror("Failed to set up suid guard.");
        return 1;
    }
    const char* pidsarg = "";
    if (argc >= 2) pidsarg = argv[1];
    auto pids_to_skip = parse_pids(pidsarg);
    for (pid_t pid: pids_to_skip) {
        std::cout << pid << " ";
    }
    std::cout << std::endl;
    for (int i = 0; i < 5; i++) {
        ps_all(false, pids_to_skip);
        usleep(ps_interval_ms*1000);
    }
    return 0;
}