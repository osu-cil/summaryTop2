#!/bin/bash
#
# summaryTop_agent - Run the st_agent.sh script to continuously collect info on system usage.
#
# chkconfig: 345 90 10
# description: Run the st_agent.sh script to continuously collect info on system usage.

### BEGIN INIT INFO
# Provides: summaryTop_agent
# Required-Start: $remote_fs
# Required-Stop: $remote_fs
# Default-Start: 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start st_agent.sh for per-user system usage monitoring.
# Description: Run the st_agent.sh script to continuously collect info on per-user system usage.
### END INIT INFO

# Configuration
SCRIPT_PATH="/cil/scripts/summaryTop2/summaryTop_agent/st_agent.sh"
STUSER=$STA_USER  # set with envsubst before install
DESC="summaryTop_agent"
PIDFILE="/var/run/summaryTop_agent.pid"

. /etc/rc.d/init.d/functions

errecho() {
    echo "$@" >&2
}

errdie() {
    errecho "$1"
    exit "${2:-1}"
}

start() {
    echo "Starting $DESC..."
    if [ ! -x "$SCRIPT_PATH" ]; then
        errdie "Error: $SCRIPT_PATH not found or not executable."
    fi

    if [ -z "$STUSER" ] \
        || ! id "$STUSER" &>/dev/null \
        || [ "$(id -u "$STUSER")" -eq 0 ] ; then
        errdie "Error: No user or invalid user specified."
    fi
    daemon --user "$STUSER" "$SCRIPT_PATH" >> /var/log/summaryTop_agent.log 2>&1 &

    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
        pgrep -f "$SCRIPT_PATH" > "$PIDFILE"
        success
    else
        errecho "Failed to start $DESC. Check logs for details."
        failure
    fi
    echo
    return $RETVAL
}

stop() {
    echo "Stopping $DESC..."
    killproc "$SCRIPT_PATH"
    RETVAL=$?
    if [ $RETVAL -eq 0 ]; then
        rm -f "$PIDFILE"
        success
    else
        failure
    fi
    echo
}

restart() {
    stop
    start
}

status_() {
    status $DESC
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        restart
        ;;
    status)
        status_
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
        ;;
esac
